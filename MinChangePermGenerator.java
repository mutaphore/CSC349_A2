import java.util.*;

public class MinChangePermGenerator {
   private String seq;
   private ArrayList<String> minList;

   public MinChangePermGenerator(String letters) {
      this.seq = letters;
   }

   public ArrayList<String> getPermutations() {
      ArrayList<String> temp = new ArrayList<String>();
      StringBuilder sb;
      String word;

      //Go through each character in the sequence
      for (int i = 0; i < seq.length(); i++) {
         minList = new ArrayList<String>();

         if (temp.size() == 0)
            minList.add(Character.toString(seq.charAt(i)));
         else {
            //For each permutation
            for (int p = 0; p < temp.size();) {
               //Insert from right to left
               word = temp.get(p);
               for (int j = word.length(); j >= 0; j--) {
                  sb = new StringBuilder(word);
                  minList.add(sb.insert(j, seq.charAt(i)).toString());
               }
               p++;
               //Insert from left to right
               if (p < temp.size()) {
                  word = temp.get(p);
                  for (int k = 0;  k <= word.length(); k++) {
                     sb = new StringBuilder(word);
                     minList.add(sb.insert(k, seq.charAt(i)).toString());
                  }
                  p++;
               }
            }
         }
         temp = new ArrayList<String>(minList);   //Copy minList to temp
      }

      return minList;
   }
}
