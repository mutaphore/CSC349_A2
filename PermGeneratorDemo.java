import java.util.*;

public class PermGeneratorDemo {

   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      String letters = new String();
      ArrayList<String> lexList, minList;
      ListIterator<String> itr;

      System.out.print("Enter string of distinct lower case letters: ");
      letters = input.nextLine();

      LexPermGenerator lexPerm = new LexPermGenerator(letters);
      lexList = lexPerm.getPermutations();

      System.out.println("The permutations of the letters from abc in lexigraphic order are: ");
      itr = lexList.listIterator();
      while (itr.hasNext())
         System.out.println(itr.next());

      System.out.println();

      MinChangePermGenerator minPerm = new MinChangePermGenerator(letters);
      minList = minPerm.getPermutations();

      System.out.println("The permutations of the letters from abc with “minimum-change” are: ");
      itr = minList.listIterator();
      while (itr.hasNext())
         System.out.println(itr.next());

      System.out.println();
   }
}
