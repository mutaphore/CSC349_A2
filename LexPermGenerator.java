import java.util.*;

public class LexPermGenerator {
   private String seq;
   private ArrayList<String> lexList;

   public LexPermGenerator(String letters) {
      this.seq = letters;
   }

   public ArrayList<String> getPermutations() {
      return LexPerm(this.seq);
   }

   private ArrayList<String> LexPerm(String seq) {
      ArrayList<String> lexList = new ArrayList<String>(), temp;
      String word;
      StringBuilder sb;
      char chr;

      if (seq != null) {
         for (int i = 0; i < seq.length(); i++) {
            sb = new StringBuilder(seq);
            word = sb.deleteCharAt(i).toString();   //Remove ith char from seq
            chr = seq.charAt(i);

            temp = LexPerm(word);   //Recursive call

            //Scan through each permutation and add removed char back
            for (int j = 0; j < temp.size(); j++) {
               sb = new StringBuilder(temp.get(j));
               temp.set(j, sb.insert(0, chr).toString());
            }

            if (temp.size() == 0)
               lexList.add(Character.toString(chr));

            lexList.addAll(temp);
         }
      }

      return lexList;
   }
}
